export const paths = {
  index: "/",
  electronics:{
    laptop: {
      macbook: {
        index: "/macbook",
      },
      thinkpad: {
        index: "/thinkpad",
      },
      asus: {
        index: "/asus",
      },
    },
    mobile: {
      iphone: {
        index: "/iphone",
      },
      samsung: {
        index: "/samsung",
      },
    },
    accessories: {
      storage: {
        sdcard: {
          index: "/sdcard",
        },
        usb: {
          index: "/usb",
        },
        hdd: {
          index: "/hdd",
        },
      },
      cables: {
        apple: {
          index: "/apple",
        },
        ugreen: {
          index: "/ugreen",
        },
      },
    },
  },
  401: "/401",
  500: "/500",
};
