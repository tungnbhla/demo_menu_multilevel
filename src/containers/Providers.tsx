// app/providers.tsx
"use client";

import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import { queryClient } from "@/config/react-query";
import { QueryClientProvider } from "@tanstack/react-query";
import { Meta } from "./Meta";
import theme from "@/config";

export function Providers({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Meta title="Dev Wardrobe" description="DevWardrobe" />
      <QueryClientProvider  client={queryClient}>
        <CssBaseline />
        <ThemeProvider theme={theme}>{children}</ThemeProvider>
      </QueryClientProvider>
    </>
  );
}
