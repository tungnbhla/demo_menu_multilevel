import { paths } from "@/paths";
import PersonIcon from "@mui/icons-material/Person";
import SportsEsportsIcon from "@mui/icons-material/SportsEsports";
import Chip from "@mui/material/Chip";
import SvgIcon from "@mui/material/SvgIcon";
import type { ReactNode } from "react";
import { useMemo } from "react";

export interface Item {
  disabled?: boolean;
  external?: boolean;
  icon?: ReactNode;
  items?: Item[];
  label?: ReactNode;
  path?: string;
  title: string;
}

export interface Section {
  items: Item[];
  subheader?: string;
}

export const useSections = () => {
  return useMemo(() => {
    return [
      {
        subheader: "Electronics",
        items: [
          {
            title: "Laptop",
            icon: (
              <SvgIcon fontSize="small">
                <PersonIcon />
              </SvgIcon>
            ),
            items: [
              {
                title: "Macbook",
                path: paths.electronics.laptop.macbook.index,
                label: <Chip color="success" label="Sale" size="small" />,
              },
              {
                title: "Thinkpad",
                path: paths.electronics.laptop.thinkpad.index,
                label: <Chip color="error" label="New" size="small" />,
              },
              {
                title: "Asus",
                path: paths.electronics.laptop.asus.index,
                label: <Chip color="error" label="New" size="small" />,
              },
            ],
          },
          {
            title: "Mobile",
            icon: (
              <SvgIcon fontSize="small">
                <SportsEsportsIcon />
              </SvgIcon>
            ),
            label: <Chip color="primary" label="Soon" size="small" />,
            items: [
              {
                title: "iPhone",
                path: paths.electronics.mobile.iphone.index,
                label: <Chip color="error" label="New" size="small" />,
              },
              {
                title: "Samsung",
                path: paths.electronics.mobile.samsung.index,
                label: <Chip color="error" label="New" size="small" />,
              },
            ],
          },
          {
            title: "Accessories",
            icon: (
              <SvgIcon fontSize="small">
                <SportsEsportsIcon />
              </SvgIcon>
            ),
            label: <Chip color="primary" label="Soon" size="small" />,
            items: [
              {
                title: "Storage",
                items: [
                  {
                    title: "SDcard",
                    path: paths.electronics.accessories.storage.sdcard.index,
                    label: <Chip color="error" label="New" size="small" />,
                  },
                  {
                    title: "USB",
                    path: paths.electronics.accessories.storage.usb.index,
                    label: <Chip color="error" label="New" size="small" />,
                  },
                  {
                    title: "HDD",
                    path: paths.electronics.accessories.storage.hdd.index,
                    label: <Chip color="error" label="New" size="small" />,
                  },
                ],
              },
              {
                title: "Cables",
                items: [
                  {
                    title: "Apple",
                    path: paths.electronics.accessories.cables.apple.index,
                    label: <Chip color="error" label="New" size="small" />,
                  },
                  {
                    title: "Ugreen",
                    path: paths.electronics.accessories.cables.apple.index,
                    label: <Chip color="error" label="New" size="small" />,
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        subheader:"Cars",
        items: [
          {
            title: "SUV",
            items: [
              {
                title: "Honda",
                path: paths.electronics.accessories.storage.sdcard.index,
                label: <Chip color="error" label="New" size="small" />,
              },
              {
                title: "Ferrari",
                path: paths.electronics.accessories.storage.usb.index,
                label: <Chip color="error" label="New" size="small" />,
              },
            ],
          },
          {
            title:'Sedan',
            label: <Chip color="info" label="Soon" size="small" />,
          },
          {
            title:'Sports ',
            label: <Chip color="info" label="Soon" size="small" />,
          }
        ],
      }
    ];
  }, []);
};
