import type { Theme } from "@mui/material";
import { styled } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import Box from "@mui/system/Box";
import dynamic from "next/dynamic";
import PropTypes from "prop-types";
import type { FC, ReactNode } from "react";
import { useEffect, useState } from "react";

import type { Section } from "../config";
import { useMobileNav } from "./use-mobile-nav";

const SideNav = dynamic(() => import("./side-nav"), { ssr: false });
const TopNav = dynamic(() => import("./top-nav"), { ssr: false });

const SIDE_NAV_WIDTH: number = 280;

const VerticalLayoutRoot = styled("div")(({ theme, style }) => ({
  display: "flex",
  flex: "1 1 auto",
  maxWidth: "100%",
  [theme.breakpoints.up("lg")]: {
    // @ts-ignore
    paddingLeft: `${style?.paddingLeftCustom || 0}px`,
  },
  [theme.breakpoints.down("lg")]: {
    paddingLeft: `0px`,
  },
}));

const VerticalLayoutContainer = styled("div")({
  display: "flex",
  flex: "1 1 auto",
  flexDirection: "column",
  width: "100%",
  padding: "16px",
});

interface VerticalLayoutProps {
  children?: ReactNode;
  sections?: Section[];
}

export const VerticalLayout: FC<VerticalLayoutProps> = (props) => {
  const { children, sections } = props;
  const lgUp = useMediaQuery((theme: Theme) => theme.breakpoints.up("lg"), {
    noSsr: false,
  });
  const mobileNav = useMobileNav();

  const [paddingNav, setPadding] = useState(0);

  useEffect(() => {
    setPadding(SIDE_NAV_WIDTH);
  }, []);

  return (
    <Box height="100%" position="relative">
      <TopNav onMobileNavOpen={mobileNav.handleOpen} />
      {lgUp && (
        <SideNav sections={sections} paddingNav={paddingNav} />
      )}
      
      {/* @ts-ignore */}
      <VerticalLayoutRoot id={"xxx"} style={{ paddingLeftCustom: paddingNav }}>
        <VerticalLayoutContainer>{children}</VerticalLayoutContainer>
      </VerticalLayoutRoot>
    </Box>
  );
};

VerticalLayout.propTypes = {
  children: PropTypes.node,
  sections: PropTypes.array,
};
