import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Link from "next/link";
import { usePathname } from "next/navigation";
import PropTypes from "prop-types";
import type { FC } from "react";

import type { Section } from "../config";
import { SideNavSection } from "./side-nav-section";
import { paths } from "@/paths";
import { useTheme } from "@mui/material/styles";

interface SideNavProps {
  sections?: Section[];
  paddingNav: number;
}

const SideNav: FC<SideNavProps> = (props) => {
  const { sections = [], paddingNav = 0 } = props;

  const pathname = usePathname();

  const theme = useTheme();

  return (
    <Box height="100%">
      <Drawer
        anchor="left"
        open
        PaperProps={{
          sx: {
            backgroundColor: "transparent",
            borderRightColor: theme.palette.secondary.main,
            borderRightStyle: "solid",
            borderRightWidth: 0,
            color: theme.palette.primary.main,
            width: `${paddingNav}px`,
            height: "100",
            overflowY: "hidden",
          },
        }}
        variant="permanent"
      >
        <Stack sx={{ height: "100%", background: theme.palette.secondary.main }} height="100%">
          <Stack alignItems="center" direction="row" spacing={2} sx={{ p: 1 }}>
            <Box
              component={Link}
              href={paths.index}
              sx={{
                display: "flex",
                textDecoration: "none",
                alignItems: "center",
              }}
            >
              {/* <Image
                width={70}
                height={60}
                src={"/img/logo.png"}
                alt=""
              /> */}
              <Typography
                color={theme.palette.primary.main}
                fontWeight={"bolder"}
                variant={"h4"}
              >
                Genuine
              </Typography>
            </Box>
          </Stack>

          <Box display="flex" height="100%">
            <Stack
              component="nav"
              spacing={2}
              sx={{
                flexGrow: 1,
                px: 2,
              }}
              height="100%"
            >
              {sections.map((section, index) => (
                <SideNavSection
                  items={section.items}
                  key={index}
                  pathname={pathname}
                  subheader={section.subheader}
                />
              ))}
            </Stack>
          </Box>
        </Stack>
      </Drawer>
    </Box>
  );
};

SideNav.propTypes = {
  sections: PropTypes.array,
};

export default SideNav;
