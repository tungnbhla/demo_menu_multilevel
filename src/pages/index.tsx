import { Layout } from "@/layouts";
import styles from "../styles/Home.module.css";
import { ReactNode } from "react";
import RefChart from "@/components/Chart/Refchart";
import { rearrangeLinkedList } from "@/utils/convertString";

const Index = () => {

  const linkedList = [1, 2, 3, 4, 5, 6, 7, 8];

  return (
    <div className={styles.container}>
      Dây là kiểu thiết kế menu có thể sử dụng lại khi cần thêm tính năng mới
      chỉ cần thêm vào config.tsx của layout sẽ tự render theme tính năng mới.
      <div>
        {" "}
        Khi cần menu có nhiều cấp trong file config chỉ cần thêm item trong từng
        cấp mà không cần chỉnh sửa lại ở file thiết kế nên menu nó đã tự dộng
        dynamic
      </div>
      <div style={{ height: "400px" }}>
        <RefChart />
      </div>
      <div style={{ marginTop: "100px" }}>Convert String 1,2,3,4,5,6,7,8 {'=>'} {(rearrangeLinkedList(linkedList)).join(",")}</div>
    </div>
  );
};
Index.getLayout = (page: ReactNode) => <Layout>{page}</Layout>;

export async function getStaticProps() {
  return {
    props: {},
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 60 * 60 * 24, // In days
  };
}

export default Index;
