/* eslint-disable no-shadow */
import React from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
} from "recharts";

const RefChart: React.FC = () => {
  const items = {
    hba1c: [
      { pv: "00:00", uv: 10 },
      { pv: "09:00", uv: 100 },
      { pv: "09:15", uv: 105 },
      { pv: "09:30", uv: 130 },
      { pv: "09:45", uv: 125 },
      { pv: "11:00", uv: 102 },
      { pv: "12:00", uv: 102 },
      { pv: "14:00", uv: 182 },
      { pv: "16:00", uv: 192 },
      { pv: "17:00", uv: 182 },
      { pv: "18:00", uv: 102 },
      { pv: "20:00", uv: 122 },
      { pv: "22:00", uv: 132 },
      { pv: "24:00", uv: 202 },
    ],
    sub_meals: ["09:04", "09:30", "11:23", "15:44", "21:33"],
    primary_meals: ["12:05", "19:10"],
  };

  const CustomTooltip = (data: { active: any; payload: any }) => {
    const { active, payload } = data;
    if (active && payload && payload.length) {
      return <div>{Number(payload[0].value)}</div>;
    }
    return null;
  };

  const CustomizedDot: React.FC<{ cx: any; cy: any }> = ({ cx, cy }) => {
    return (
      <svg
        x={cx - 10}
        y={cy - 10}
        width={20}
        height={20}
        fill="red"
        viewBox="0 0 1024 1024"
      >
        <path d="M512 1009.984c-274.912 0-497.76-222.848-497.76-497.76s222.848-497.76 497.76-497.76c274.912 0 497.76 222.848 497.76 497.76s-222.848 497.76-497.76 497.76zM340.768 295.936c-39.488 0-71.52 32.8-71.52 73.248s32.032 73.248 71.52 73.248c39.488 0 71.52-32.8 71.52-73.248s-32.032-73.248-71.52-73.248zM686.176 296.704c-39.488 0-71.52 32.8-71.52 73.248s32.032 73.248 71.52 73.248c39.488 0 71.52-32.8 71.52-73.248s-32.032-73.248-71.52-73.248zM772.928 555.392c-18.752-8.864-40.928-0.576-49.632 18.528-40.224 88.576-120.256 143.552-208.832 143.552-85.952 0-164.864-52.64-205.952-137.376-9.184-18.912-31.648-26.592-50.08-17.28-18.464 9.408-21.216 21.472-15.936 32.64 52.8 111.424 155.232 186.784 269.76 186.784 117.984 0 217.12-70.944 269.76-186.784 8.672-19.136 9.568-31.2-9.12-40.096z" />
      </svg>
    );
  };

  return (
    <ResponsiveContainer height="100%" minHeight={"300px"}>
      <AreaChart
        width={250}
        height={250}
        data={items.hba1c}
        margin={{
          top: 0,
          right: 0,
          left: 0,
          bottom: 0,
        }}
      >
        <YAxis
          orientation="left"
          tick={{ stroke: "#718096", strokeWidth: 0.1 }}
          tickLine={false}
          axisLine={true}
          type="number"
          tickCount={5}
          padding={{ bottom: 0 }}
          fontSize={"14px"}
          fontWeight={"medium"}
          stroke="#718096"
        />
        <XAxis
          orientation="top"
          dataKey="pv"
          tickLine={true}
          axisLine={true}
          padding={{ right: 1 }}
          interval={"preserveEnd"}
          stroke="#718096"
        />
        <defs>
          <linearGradient id="upTrend" x1="0" y1="0" x2="0" y2="1">
            <stop offset="50%" stopColor="rgba(26, 196, 134, 0.16)" />
            <stop
              offset="100%"
              stopColor="rgba(26, 196, 134, 0.02)"
              stopOpacity={0}
            />
          </linearGradient>
        </defs>
        <defs>
          <linearGradient id="downTrend" x1="0" y1="0" x2="0" y2="1">
            <stop offset="50%" stopColor="#F56565" />
            <stop
              offset="100%"
              stopColor="rgba(245, 101, 101, 0)"
              stopOpacity={0}
            />
          </linearGradient>
        </defs>
        <CartesianGrid
          horizontal={false}
          vertical={true}
          stroke={"#262F3D"}
          strokeWidth={1}
          strokeDasharray="2 2"
        />
        {
          <Tooltip
            cursor={{ stroke: "transparent", strokeWidth: 1 }}
            content={<CustomTooltip active={undefined} payload={undefined} />}
          />
        }
        <Area
          type="monotone"
          dataKey="uv"
          cursor={"pointer"}
          stroke={"#1AC486"}
          fill={"url(#upTrend)"}
          dot={<CustomizedDot cx={undefined} cy={undefined} />}
          activeDot={{ stroke: "#1AC486", strokeWidth: 2, r: 5 }}
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default RefChart;
