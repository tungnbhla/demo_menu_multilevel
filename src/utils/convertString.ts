export const rearrangeLinkedList= (linkedList: string | any[]) => {
    let p1 = 0, p2 = linkedList.length - 1;
    const result = [];

    while (p1 <= p2) {
        result.push(linkedList[p1]);
        if (p1 !== p2) {
            result.push(linkedList[p2]);
        }
        p1++;
        p2--;
    }

    return result;
}