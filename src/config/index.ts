// /src/configs/muiTheme.ts

import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#000000',
    },
    secondary: {
      main: '#E9D8FD',
    },
  },
});

export default theme;
